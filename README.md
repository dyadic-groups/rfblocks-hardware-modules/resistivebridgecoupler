# A Directional Bridge

## Features

- 16dB nominal coupling.
- 1.75dB nominal insertion loss.
- Directivity: >30dB for f < 1.0 GHz, >20dB for f < 4 GHz.
- Compact size and easily fabricated using standard PCB techniques.

## Documentation

Full documentation for the bridge is available at
[RF Blocks](https://rfblocks.org/boards/Directional-Bridge.html)

## License

[CERN-OHL-W v2.](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2)
